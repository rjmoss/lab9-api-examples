#
# This file is the lab9-api-examples recipe.
#

SUMMARY = "VCU Relaunch Lab #9 API Example Applications"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://hellovcu"
SRC_URI += "file://lab9-soft-tpg-example"
SRC_URI += "file://lab9-gst-state-example"
SRC_URI += "file://lab9-dynamic-tpg-example"

S = "${WORKDIR}"

DEPENDS = "gstreamer1.0 gstreamer1.0-plugins-base"

inherit pkgconfig

do_compile() {
	     oe_runmake -C hellovcu
	     oe_runmake -C lab9-soft-tpg-example
	     oe_runmake -C lab9-gst-state-example
	     oe_runmake -C lab9-dynamic-tpg-example
}

do_install() {
		# Install 3 example applications
		install -d ${D}${bindir}
		install -m 0755 hellovcu/hellovcu ${D}${bindir}
		install -m 0755 lab9-soft-tpg-example/lab9-soft-tpg-example ${D}${bindir}
		install -m 0755 lab9-gst-state-example/lab9-gst-state-example ${D}${bindir}
		install -m 0755 lab9-dynamic-tpg-example/lab9-dynamic-tpg-example ${D}${bindir}

		# Install helper scripts
		install -m 0755 lab9-gst-state-example/scripts/lab9_init_abox_1080p.sh ${D}${bindir}
		install -m 0755 lab9-gst-state-example/scripts/lab9_init_abox_4k.sh ${D}${bindir}
		install -m 0755 lab9-gst-state-example/scripts/lab9_init_shield_1080p.sh ${D}${bindir}
		install -m 0755 lab9-gst-state-example/scripts/lab9_init_shield_4k.sh ${D}${bindir}
		install -m 0755 lab9-gst-state-example/scripts/lab9_set_hdmi_4kp60.sh ${D}${bindir}		
}
