/* LAB9 GStreamer Soft Test Pattern Example Main Header File */

#ifndef LAB9SOFTTPGEXAMPLE_H_
#define LAB9SOFTTPGEXAMPLE_H_

#include <stdbool.h>
#include <gst/gst.h>
#include "globals.h"

/* Function Prototypes */
void usage (const char *argv0);
void showMessages (GstBus *bus, GstClockTime wait, GstMessageType types);

/* Static Pipeline Definitions */
/* 1080P Pipelines             */
const char *displayPortPipeline1080P = "videotestsrc pattern=0 "\
"! video/x-raw, width=1920, height=1080, framerate=15/1 "\
"! queue "\
"! kmssink bus-id=\"fd4a0000.zynqmp-display\" "\
" fullscreen-overlay=true";

const char *hdmiPipeline1080P = "videotestsrc pattern=0 "\
"! video/x-raw, width=1920, height=1080, framerate=15/1 "\
"! queue "\
"! kmssink bus-id=\"a0070000.v_mix\" "\
" fullscreen-overlay=true";

/* 4K    Pipelines             */
const char *displayPortPipeline4K = "videotestsrc pattern=0 "\
"! video/x-raw, width=3840, height=2160, framerate=5/1 "\
"! queue "\
"! kmssink bus-id=\"fd4a0000.zynqmp-display\" "\
" fullscreen-overlay=true";

const char *hdmiPipeline4K = "videotestsrc pattern=0 "\
"! video/x-raw, width=3840, height=2160, framerate=5/1 "\
"! queue "\
"! kmssink bus-id=\"a0070000.v_mix\" "\
" fullscreen-overlay=true";

#endif