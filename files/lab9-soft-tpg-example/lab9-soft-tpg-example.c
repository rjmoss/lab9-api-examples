/*
* Copyright (C) 2013 - 2016  Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in this
* Software without prior written authorization from Xilinx.
*
*/

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>

#include <gst/gst.h>

/* To turn on debug print messages, define DEBUG */
//#define DEBUG

#include "lab9-soft-tpg-example.h"

/* Flags set by '--display-dp' and '--display-hdmi' options */
int display_dp = 0;
int display_hdmi = 0;

/* Flags set by '--1080p' and '--4k' options */
int resolution_1080p = 0;
int resolution_4k = 0;

/* Program Name */
const char *prog;

/* Command line long option formatting */
static struct option long_options[] = 
{
	{"display-dp", no_argument, &display_dp, 1},
	{"display-hdmi", no_argument, &display_hdmi, 1},
    {"1080p", no_argument, &resolution_1080p, 1},
    {"1080P", no_argument, &resolution_1080p, 1},
    {"4k", no_argument, &resolution_4k, 1},
    {"4K", no_argument, &resolution_4k, 1},
	{"help", no_argument, NULL, 'h'},
	{NULL, 0, NULL, 0}
};

int main (int argc, char **argv)
{
	int opt;
	char *d_optarg, *r_optarg;
    const char *selected;

    prog = argv[0];

    /* Selected Default Display Type */
    displaytype_t displaytype = NO_DISPLAY;

    /* Select Default Resolution */
    resolutiontype_t resolution = NO_RESOLUTION;

    fprintf (stdout, "%s: Soft Test Pattern Generation VCU Relaunch Session #9\n", prog);

    /* Parse command line options */
    while ((opt = getopt_long(argc, argv, "h", long_options, NULL)) != -1) {
    	switch (opt) {
    	case 'h': /* Command Help */
    		usage (argv[0]);
    		exit (EXIT_SUCCESS);
            break;

    	default: /* Long Options */
            debug_print ("%s: DEBUG: Long Option=[%c]\n", prog, opt);
            if (display_dp){
                debug_print ("%s: DEBUG: Long option --display-dp set\n", prog);
                displaytype = DISPLAYPORT;
                display_dp = 0;
            } else if (display_hdmi){
                debug_print ("%s: DEBUG: Long option --display-hdmi set\n", prog);
                displaytype = HDMI;
                display_hdmi = 0;
            } else if (resolution_1080p){
                debug_print ("%s: DEBUG: Long option --resolution_1080p set\n", prog);
                resolution = RESOLUTION1080P;
                resolution_1080p = 0;
            } else if (resolution_4k){
                debug_print ("%s: DEBUG: Long option --resolution_4k set\n", prog);
                resolution = RESOLUTION4K;
                resolution_4k = 0;
            } else {
                debug_print ("%s: DEBUG: Invalid option (%c)\n", prog, opt);
                usage(argv[0]);
                exit(EXIT_FAILURE);
            }
            break;
        }
    }

    /* Check that a resolution and display interface have been specified */
    if ((displaytype == NO_DISPLAY) || (resolution == NO_RESOLUTION)){
        fprintf(stderr, "%s: Please select a display interface and resolution\n", prog);
        usage (argv[0]);
        exit (EXIT_FAILURE);
    }

    /* Initialize GStreamer */
    gst_init (NULL, NULL);

    /* Select the output pipeline based on the display and source configuration */
    switch (displaytype) {

    case DISPLAYPORT:
        debug_print ("%s: DEBUG: DisplayPort selected: Fixed ID = fd4a0000.zynqmp-display\n", prog);
        switch (resolution) {
        case RESOLUTION1080P:
            debug_print ("%s: DEBUG: DisplayPort+1080p selected\n", prog);
            selected = displayPortPipeline1080P;
            break;

        case RESOLUTION4K:
            debug_print ("%s: DEBUG: DisplayPort+4K selected\n", prog);
            selected = displayPortPipeline4K;
            break;

        case NO_RESOLUTION:
        default:
            debug_print ("%s: DEBUG: Undefined resolution!\n", prog);
            usage (argv[0]);
            exit (EXIT_FAILURE);
        break;
        }
        break;

    case HDMI:
        debug_print ("%s: DEBUG: HDMI selected: Fixed ID = a0070000.v_mix\n", prog);
        switch (resolution) {
        case RESOLUTION1080P:
            debug_print ("%s: DEBUG: HDMI+1080p selected\n", prog);
            selected = hdmiPipeline1080P;
            break;

        case RESOLUTION4K:
            debug_print ("%s: DEBUG: HDMI+4K selected\n", prog);
            selected = hdmiPipeline4K;
            break;
        
        case NO_RESOLUTION:
        default:
            debug_print ("%s: DEBUG: Undefined resolution!\n", prog);
            usage (argv[0]);
            exit (EXIT_FAILURE);
        break;
        }
        break;

    case NO_DISPLAY:
    default:
        debug_print ("%s: DEBUG: Undefined display type!\n", prog);
        usage (argv[0]);
        exit (EXIT_FAILURE);
    }


    /* Initialize GStreamer Pipeline Control */
    GstElement *pipeline = NULL;
    GError *err = NULL;

    /* Parse selected pipeline */
    pipeline = gst_parse_launch(selected, &err);
    
    if (!pipeline) {
        fprintf (stderr, "%s: Error in parsing pipeline [%s]\n", prog, err->message);
        exit (EXIT_FAILURE);
    }

    if(err != NULL){
        fprintf (stderr, "%s: Error message set parsing pipeline [%s]\n", prog, err->message);
        g_error_free (err);
        exit (EXIT_FAILURE);
    }

    GstBus *bus = NULL;

    /* Get the pipeline bus reference and Clock */
    bus = gst_element_get_bus (pipeline);

    /* Check bus for messages */
    showMessages( bus, GST_SECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED);

    GstStateChangeReturn sret;
    GstClock *clock = NULL;

    /* Get Initial State */
    debug_print ("%s: DEBUG: Getting initial state after parse_launch\n", prog);
    sret = gst_element_get_state(pipeline, NULL, NULL, GST_CLOCK_TIME_NONE);
    clock = gst_element_get_clock (pipeline);

    /* Check bus for messages */
    showMessages (bus, GST_SECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED);

    /* Start the pipeline */
    gst_element_set_state (pipeline, GST_STATE_PLAYING);

    /* Check bus for messages */
    showMessages (bus, GST_SECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED);

    fprintf (stdout, "%s: *** Press the \'Q\' key followed by ENTER, or CTRL-C to exit\n", prog);

    /* Wait for end of stream */
    //showMessages (bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | GST_MESSAGE_STATE_CHANGED);
    /* Instead, Wait for input from command line and close out pipeline properly */
    char input;
    bool exit_loop = false;
    do {
        input = getchar();
        if ((input == 'q') || (input == 'Q')) {
            exit_loop = true;
        }
        /* Flush the console input buffer */
        while ((input = getchar()) != EOF && (input != '\n'));

    } while ( !exit_loop );

    /* Put Pipeline in PAUSED state first */
    /* This restores previous CRTC configuration if overridden */
    debug_print ("%s: DEBUG: Pausing Pipeline\n", prog);
    gst_element_set_state (pipeline, GST_STATE_PAUSED);

    showMessages (bus, GST_SECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED);

    /* Stop Pipeline */
    debug_print ("%s: DEBUG: Stoppping Pipeline\n", prog);
    gst_element_set_state (pipeline, GST_STATE_NULL);

    showMessages (bus, GST_SECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED);

    /* Free GStreamer resources */
    if (clock != NULL) {
        gst_object_unref (clock);
    }
    if (bus != NULL) {
        gst_object_unref (bus);
    }
    if (pipeline != NULL) {
        gst_object_unref (pipeline);
    }	
    exit(EXIT_SUCCESS);
}

/* Show pipeline messages matching the message filter */
void showMessages (GstBus *bus, GstClockTime wait, GstMessageType types)
{
    GstMessage *msg = NULL;

    do
    {
        msg = gst_bus_timed_pop_filtered (bus, wait, types);
        if (msg != NULL){
            switch (GST_MESSAGE_TYPE (msg)) {
            case GST_MESSAGE_ERROR:
                {
                    GError *err = NULL;
                    gchar *debug_message = NULL;
                    gst_message_parse_error (msg, &err, &debug_message);
                    fprintf (stderr, "%s: ERROR from element %s: %s\n", 
                        prog, GST_OBJECT_NAME(msg->src), err->message);
                    fprintf (stderr, "%s: - Debug Information  : %s\n", 
                        prog, (debug_message) ? debug_message : "N/A");
                    g_error_free (err);
                    g_free (debug_message);
                }
                break;

            case GST_MESSAGE_EOS:
                fprintf (stdout, "%s: %s: EOS reached\n", prog, GST_OBJECT_NAME (msg->src));
                break;

            case GST_MESSAGE_STATE_CHANGED:
                {
                    GstState old, new;
                    gst_message_parse_state_changed (msg, &old, &new, NULL);
                    fprintf (stdout, "%s: %s: changed state from %s to %s.\n", 
                        prog, GST_OBJECT_NAME (msg->src), gst_element_state_get_name (old), 
                        gst_element_state_get_name (new));
                }
                break;
            }
        }
    } while (msg != NULL);
}

/* Command line usage message */
void usage (const char *argv0)
{
    fprintf (stderr, "Display a software generated test pattern on the specified display\n");
    fprintf (stderr, "Usage: %s [display] [resolution] [options]\n", argv0);
    fprintf (stderr, " Available [display] options:\n");
    fprintf (stderr, "\t --display-dp          Send test pattern to PS DisplayPort\n");
    fprintf (stderr, "\t --display-hdmi        Send test pattern to HDMI-TX through the Video Mixer\n");
    fprintf (stderr, " Available [resolution] options:\n");
    fprintf (stderr, "\t --1080p, --1080P      Use a 1080p software generated test pattern\n");
    fprintf (stderr, "\t --4k, --4K            Use a 4K software generated test pattern\n");
    fprintf (stderr, " Available [options]:\n");
    fprintf (stderr, "\t -h, --help            Print this help screen and exit\n");
}
