/* LAB9 Gstreamer Soft Test Pattern Example Global Header File */

#ifndef LAB9SOFTTPGEXAMPLE_GLOBALS_H_
#define LAB9SOFTTPGEXAMPLE_GLOBALS_H_

/* Enable debug print messages in application */
#ifdef DEBUG
    #define debug_print(fmt, ...) fprintf (stderr, "%s:%s:%d: "fmt, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
    #define debug_print(fmt, ...)
#endif

/* Type Definitions */
typedef enum {NO_DISPLAY, DISPLAYPORT, HDMI} displaytype_t;
typedef enum {NO_RESOLUTION, RESOLUTION1080P, RESOLUTION4K} resolutiontype_t;

/* Constants */
extern const char *displayportPipeline1080P;
extern const char *hdmiPipeline1080P;
extern const char *displayportPipeline4K;
extern const char *hdmiPipeline4k;

/* Global Variables */
extern int display_dp;
extern int display_hdmi;
extern int resolution_1080p;
extern int resolution_4k;
extern const char *prog;

/* Global Function Prototypes */
extern void showMessages (GstBus *bus, GstClockTime wait, GstMessageType types);
#endif