/*
* Copyright (C) 2013 - 2016  Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in this
* Software without prior written authorization from Xilinx.
*
*/

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>

#include <gst/gst.h>

/* To turn on debug print messages, define DEBUG */
//#define DEBUG

#include "lab9-gst-state-example.h"

/* Flags set by '--rx-1080p' and '--rx-4k' options */
int rxresolution_1080p = 0;
int rxresolution_4k = 0;

/* Program Name */
const char *prog;

/* Command line long option formatting */
static struct option long_options[] = 
{
    {"rx-1080p", no_argument, &rxresolution_1080p, 1},
    {"rx-1080P", no_argument, &rxresolution_1080p, 1},
    {"rx-4k", no_argument, &rxresolution_4k, 1},
    {"rx-4K", no_argument, &rxresolution_4k, 1},
    {"help", no_argument, NULL, 'h'},
	{NULL, 0, NULL, 0}
};

int main (int argc, char **argv)
{
	int opt;
    const char *inputselect;

    prog = argv[0];

    /* Selected HDMI-RX Input Resolution */
    rxresolutiontype_t rxresolution = NO_RXRESOLUTION;

    fprintf (stdout, "%s: GStreamer State Control Example - VCU Relaunch Session #9\n", prog);

    /* Parse command line options */
    while ((opt = getopt_long(argc, argv, "h", long_options, NULL)) != -1) {
    	switch (opt) {
        case 'h': /* Command Help */
            usage (argv[0]);
            exit (EXIT_SUCCESS);
            break;

        default: /* Long Options */
            debug_print ("%s: DEBUG: Long Option=[%c]\n", prog, opt);
            if (rxresolution_1080p){
                debug_print ("%s: DEBUG: Long option --rx-1080p set\n", prog);
                rxresolution = RXRESOLUTION1080P;
                rxresolution_1080p = 0;
            } else if (rxresolution_4k){
                debug_print ("%s: DEBUG: Long option --rx-4k set\n", prog);
                rxresolution = RXRESOLUTION4K;
                rxresolution_4k = 0;
            } else {
                debug_print ("%s: DEBUG: Invalid option (%c)\n", prog, opt);
                usage (argv[0]);
                exit (EXIT_FAILURE);
            }
            break;
        }
    }

    /* Check that a HDMI Input resolution has been specified */
    if ((rxresolution == NO_RXRESOLUTION)){
        fprintf (stderr, "%s: Please select a HDMI Input resolution\n", prog);
        usage (argv[0]);
        exit (EXIT_FAILURE);
    }

    /* Select the output pipeline based on the hdmi input source configuration */
    switch (rxresolution) {

    case RXRESOLUTION1080P:
        debug_print ("%s: DEBUG: HDMI+1080p selected\n", prog);
        inputselect = hdmiPipelineHDMIRx1080p;
        break;

    case RXRESOLUTION4K:
        debug_print ("%s: DEBUG: HDMI+4K selected\n", prog);
        inputselect = hdmiPipelineHDMIRx4k;
        break;
        
    case NO_RXRESOLUTION:
        default:
            debug_print ("%s: DEBUG: Undefined HDMI Input Resolution!\n", prog);
            usage (argv[0]);
            exit (EXIT_FAILURE);
        break;
    }

    /* Initialize GStreamer */    
    gst_init (NULL, NULL);

    /***************************************************************/
    /* Initialize GStreamer Pipeline Control for the Time Overlay  */
    /***************************************************************/
    gstPipelineControl_t overlayctl;
    overlayctl.description = timeOverlayPipelineDescription;

    if ( !initPipeline (&overlayctl, timeOverlayPipeline) ){
        exit (EXIT_FAILURE);
    }

    /* Display any messages on the bus */
    showMessages (overlayctl.bus, GST_SECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED, overlayctl.description);

    /***************************************************************/
    /* Initialize GStreamer Pipeline Control for the Test Pattern  */
    /***************************************************************/
    gstPipelineControl_t tpgctl;
    tpgctl.description = tpgPipelineDescription;

    if ( !initPipeline (&tpgctl, tpgPipeline720p) ){
        exit (EXIT_FAILURE);
    }

    /* Display any messages on the bus */
    showMessages (tpgctl.bus, 100*GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED, tpgctl.description);

    /***************************************************************/
    /* Initialize GStreamer Pipeline Control for HDMI Pass Through */
    /***************************************************************/
    gstPipelineControl_t hdmictl;
    hdmictl.description = hdmiPipelineHDMIRxDescription;

    if ( !initPipeline (&hdmictl, inputselect) ){
        exit (EXIT_FAILURE);
    }

    /* Display any messages on the bus */
    showMessages (hdmictl.bus, 100*GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED, hdmictl.description);

    /* Get inital state information */
    // showStateInfo (&overlayctl);
    // showStateInfo (&tpgctl);
    // showStateInfo (&hdmictl);

    /**************************/
    /* Start the Time Overlay */
    /**************************/
    /* Start the pipelines */
    gst_element_set_state (overlayctl.pipeline, GST_STATE_PLAYING);
    /* Check bus for messages */
    showMessages (overlayctl.bus, 100*GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED, overlayctl.description);

    /**************************/
    /* Start the TPG          */
    /**************************/
    /* Start the pipelines */
    //gst_element_set_state (tpgctl.pipeline, GST_STATE_PLAYING);
    /* Check bus for messages */
    showMessages (tpgctl.bus, 100*GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED, tpgctl.description);

    /**************************/
    /* Start the HDMI Pass    */
    /**************************/
    /* Start the pipelines */
    //gst_element_set_state (hdmictl.pipeline, GST_STATE_PLAYING);
    /* Check bus for messages */
    showMessages (hdmictl.bus, 100*GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED, hdmictl.description);

    char input;
    bool exit_loop = false;
    do {
        showMenu (&overlayctl, &tpgctl, &hdmictl);

        input = getchar ();
        
        exit_loop = !processInput (&input, &overlayctl, &tpgctl, &hdmictl);

        /* Flush the console input buffer */
        while ((input = getchar ()) != EOF && (input != '\n'));

        sleep(0);

    } while ( !exit_loop );

    /* Pause any running pipelines */
    if (GST_STATE(hdmictl.pipeline) == GST_STATE_PLAYING){
        hdmictl.sret = gst_element_set_state (hdmictl.pipeline, GST_STATE_PAUSED);
        fprintf (stdout, "%s: %s: State Change Return Status (%s)\n", prog, hdmictl.description, 
            gst_element_state_change_return_get_name(hdmictl.sret));
    }
    if (GST_STATE(tpgctl.pipeline) == GST_STATE_PLAYING){
        tpgctl.sret = gst_element_set_state (tpgctl.pipeline, GST_STATE_PAUSED);
        fprintf (stdout, "%s: %s: State Change Return Status (%s)\n", prog, tpgctl.description, 
            gst_element_state_change_return_get_name(tpgctl.sret));
    }
    if (GST_STATE(overlayctl.pipeline) == GST_STATE_PLAYING){
        overlayctl.sret = gst_element_set_state (overlayctl.pipeline, GST_STATE_PAUSED);
        fprintf (stdout, "%s: %s: State Change Return Status (%s)\n", prog, overlayctl.description, 
            gst_element_state_change_return_get_name(overlayctl.sret));
    }

    /* Check the message bus for a state change message */
    showMessages (hdmictl.bus, 100*GST_MSECOND, 
        GST_MESSAGE_EOS | GST_MESSAGE_STATE_CHANGED, hdmictl.description);
    /* Check the message bus for a state change message */
    showMessages (tpgctl.bus, 100*GST_MSECOND, 
        GST_MESSAGE_EOS | GST_MESSAGE_STATE_CHANGED, tpgctl.description);
    /* Check the message bus for a state change message */
    showMessages (overlayctl.bus, 100*GST_MSECOND, 
        GST_MESSAGE_EOS | GST_MESSAGE_STATE_CHANGED, overlayctl.description);

    /* Show Final Status and Runtime Information */
    fprintf (stdout, "%s", menuDivider);
    fprintf (stdout, "** Final Pipeline Status\n");
    fprintf (stdout, "** %-15s | %-15s | %s\n", "Pipeline", "Current State", "Running Time");
    fprintf (stdout, "** %-15s | %-15s | %"GST_TIME_FORMAT "\n", 
        overlayctl.description, gst_element_state_get_name (GST_STATE(overlayctl.pipeline)), 
        GST_TIME_ARGS( computeRunningTime (&overlayctl)));
    fprintf (stdout, "** %-15s | %-15s | %"GST_TIME_FORMAT "\n", 
        tpgctl.description, gst_element_state_get_name (GST_STATE(tpgctl.pipeline)), 
        GST_TIME_ARGS( computeRunningTime (&tpgctl)));
    fprintf (stdout, "** %-15s | %-15s | %"GST_TIME_FORMAT "\n", 
        hdmictl.description, gst_element_state_get_name (GST_STATE(hdmictl.pipeline)), 
        GST_TIME_ARGS( computeRunningTime (&hdmictl)));
    fprintf (stdout, "%s", menuDivider);

    /**************************/
    /* Stop the HDMI Pass     */
    /**************************/
    /* Stop Pipelines */
    debug_print ("%s: %s: DEBUG: Stoppping Pipeline\n", prog, hdmictl.description);
    gst_element_set_state (hdmictl.pipeline, GST_STATE_NULL);

    /**************************/
    /* Stop the TPG          */
    /**************************/
    /* Stop Pipelines */
    debug_print ("%s: %s: DEBUG: Stoppping Pipeline\n", prog, tpgctl.description);
    gst_element_set_state (tpgctl.pipeline, GST_STATE_NULL);

    /**************************/
    /* Stop the Time Overlay  */
    /**************************/
    /* Stop Pipelines */
    debug_print ("%s: %s: DEBUG: Stoppping Pipeline\n", prog, overlayctl.description);
    gst_element_set_state (overlayctl.pipeline, GST_STATE_NULL);

    /***************************************************************/
    /* Free GStreamer Pipeline Resources for the HDMI Pass Through */
    /***************************************************************/
    deinitPipeline (&hdmictl);

    /***************************************************************/
    /* Free GStreamer Pipeline Resources for the TPG               */
    /***************************************************************/
    deinitPipeline (&tpgctl);

    /***************************************************************/
    /* Free GStreamer Pipeline Resources for the Time Overlay      */
    /***************************************************************/
    deinitPipeline (&overlayctl);

    exit (EXIT_SUCCESS);
}

/* Command line usage message */
void usage (const char *argv0)
{
    fprintf (stderr, "Display a software generated test pattern on the specified display\n");
    fprintf (stderr, "Usage: %s [hdmirx resolution] [options]\n", argv0);
    fprintf (stderr, " Available [hdmirx resolution] options:\n");
    fprintf (stderr, "\t --rx-1080p, --rx-1080P      HDMI Input @ 1080p60\n");
    fprintf (stderr, "\t --rx-4k, --rx-4K            HDMI Input @ 4kp60\n");
    fprintf (stderr, "Available [options]:\n");
    fprintf (stderr, "\t -h, --help            Print this help screen and exit\n");
}

/* Initialize Pipeline */
bool initPipeline (gstPipelineControl_t *ctl, const char *pipeline)
{
    bool ret = true;
    ctl->pipeline = NULL;
    ctl->err = NULL;

    /* Parse inputselect pipeline */
    ctl->pipeline = gst_parse_launch (pipeline, &ctl->err);
    
    if (!ctl->pipeline) {
        fprintf (stderr, "%s: %s: Error in parsing pipeline [%s]\n", 
            prog, ctl->description, ctl->err->message);
        ret = false;
    } else if (ctl->err != NULL){
        fprintf (stderr, "%s: %s: Error message set parsing pipeline [%s]\n", 
            prog, ctl->description, ctl->err->message);
        g_error_free (ctl->err);
        ret = false;
    } else {

        ctl->bus = NULL;

        /* Get the pipeline bus reference and Clock */
        ctl->bus = gst_element_get_bus (ctl->pipeline);

        /* Get Initial State */
        debug_print ("%s: %s: DEBUG: Getting initial state after parse_launch\n", 
            prog, ctl->description);
        ctl->sret = gst_element_get_state (ctl->pipeline, &ctl->currentState, 
            &ctl->pendingState, GST_CLOCK_TIME_NONE);

        /* Get the pipeline bus reference and Clock */
        ctl->bus = gst_element_get_bus (ctl->pipeline);
        debug_print("%s: %s: DEBUG: getting clock\n", prog, ctl->description);
        ctl->clock = gst_element_get_clock (ctl->pipeline);
    }
    return ret;
}

/* De-Initialize Pipeline */
void deinitPipeline (gstPipelineControl_t *ctl)
{
    if (ctl->clock != NULL){
        debug_print("%s: %s: Unref Clock\n", prog, ctl->description);
        gst_object_unref (ctl->clock);
    }
    if (ctl->err != NULL) {
        debug_print("%s: %s: Error message set parsing pipeline [%s]\n", 
            prog, ctl->description, ctl->err->message);
        g_error_free (ctl->err);
    }
    if (ctl->msg != NULL) {
        debug_print("%s: %s: Unref Msg\n", prog, ctl->description);
        showMessages (ctl->bus, GST_MSECOND, GST_MESSAGE_ANY, ctl->description);
    }
    if (ctl->bus != NULL) {
        debug_print("%s: %s: Unref Bus\n", prog, ctl->description);
        gst_object_unref (ctl->bus);
    }
    if (ctl->pipeline != NULL) {
        debug_print("%s: %s: Unref Pipeline\n", prog, ctl->description);
        gst_object_unref (ctl->pipeline);
    }   
}

GstClockTime computeRunningTime (gstPipelineControl_t *ctl)
{
    /* Refresh state information */
    ctl->sret = gst_element_get_state (ctl->pipeline, &ctl->currentState, 
        &ctl->pendingState, GST_CLOCK_TIME_NONE);
    GstClockTime runtime = 0;   

    switch (ctl->currentState) {
        case GST_STATE_PLAYING:
            debug_print ("%s: %s: DEBUG: Computing running time for PLAYING pipeline\n", 
                prog, ctl->description );
            runtime = (gst_clock_get_time (ctl->pipeline->clock) - 
                gst_element_get_base_time (ctl->pipeline)); 
            break;

        /*case GST_STATE_VOID_PENDING:
        case GST_STATE_NULL:
        case GST_STATE_READY:
        case GST_STATE_PAUSED: */
        default:
            debug_print ("%s: %s: DEBUG: Computing running time for NON-PLAYING pipeline\n", 
                prog, ctl->description );
            runtime = gst_element_get_start_time(ctl->pipeline);
            break;
    }
    return runtime;
}

void showStateInfo (gstPipelineControl_t *ctl)
{
    /* Refresh state information */
    ctl->sret = gst_element_get_state (ctl->pipeline, &ctl->currentState, 
        &ctl->pendingState, GST_CLOCK_TIME_NONE);
 
    /* Show formatted state information */
    /* Current State | State Change Return | */
    fprintf (stdout, "** %-20s : Current State : %-10s | State Change Return : %-10s\n", 
        ctl->description, gst_element_state_get_name (ctl->currentState), 
        gst_element_state_change_return_get_name (ctl->sret));
}

void showTimes (gstPipelineControl_t *ctl)
{
    fprintf (stdout, "%s", menuDivider);
    fprintf (stdout, "%s: %s: Time Information\n", prog, ctl->description);
    fprintf (stdout, "%s", menuDivider);

    /* Show current pipeline runtime */
    if (ctl->pipeline != NULL){
        /* Current State | State Change Return | */
        fprintf (stdout, "** %-15s : Base time    : %"GST_TIME_FORMAT "\n", 
            ctl->description,  GST_TIME_ARGS( gst_element_get_base_time (ctl->pipeline) ));
        fprintf (stdout, "** %-15s : Start time   : %"GST_TIME_FORMAT "\n", 
            ctl->description,  GST_TIME_ARGS( gst_element_get_start_time (ctl->pipeline) ));        
        fprintf (stdout, "** %-15s : Running time : %"GST_TIME_FORMAT "\n", 
            ctl->description,  GST_TIME_ARGS( computeRunningTime(ctl)));
    } else {
        fprintf (stdout, "** %-15s: Base time    : N/A\n", ctl->description);
        fprintf (stdout, "** %-15s: Start time   : N/A\n", ctl->description);
        fprintf (stdout, "** %-15s: Running time : N/A\n", ctl->description);
    }
    fprintf (stdout, "%s", menuDivider);
}

/* Show pipeline messages matching the message filter */
void showMessages (GstBus *bus, GstClockTime wait, GstMessageType types, const char *description)
{
    GstMessage *msg = NULL;

    msg = gst_bus_timed_pop_filtered (bus, wait, types);

    if (msg != NULL){
        fprintf (stdout, "%s", menuDivider);
        fprintf (stdout, "%s: %s: Message Log\n", prog, description);
        fprintf (stdout, "%s", menuDivider);       

        while (msg != NULL){

            switch (GST_MESSAGE_TYPE (msg)) {
            case GST_MESSAGE_ERROR:
                {
                    GError *err = NULL;
                    gchar *debug_message = NULL;
                    gst_message_parse_error (msg, &err, &debug_message);
                    fprintf (stderr, "%s: ERROR from element %s: %s\n", 
                        prog, GST_OBJECT_NAME(msg->src), err->message);
                    fprintf (stderr, "%s: - Debug Information  : %s\n", 
                        prog, (debug_message) ? debug_message : "N/A");
                    g_error_free (err);
                    g_free (debug_message);
                }
                break;

            case GST_MESSAGE_EOS:
                fprintf (stdout, "%s: %s: EOS reached\n", 
                    prog, GST_OBJECT_NAME (msg->src));
                break;

            case GST_MESSAGE_STATE_CHANGED:
                {
                    GstState old, new;
                    gst_message_parse_state_changed (msg, &old, &new, NULL);
                    fprintf (stdout, "%s: %s: changed state from %s to %s.\n", 
                        prog, GST_OBJECT_NAME (msg->src), gst_element_state_get_name (old), 
                        gst_element_state_get_name (new));
                }
                break;

            default:
                {
                    fprintf (stdout, "%s: %s: Message (%s) not parsed.\n", 
                        prog, GST_OBJECT_NAME (msg->src), GST_MESSAGE_TYPE_NAME (msg));
                }
                break;
            }
            gst_message_unref(msg);
            //Check for additional messages
            msg = gst_bus_timed_pop_filtered (bus, wait, types);
        } 
        fprintf (stdout, "%s\n", menuDivider);
    }
}
