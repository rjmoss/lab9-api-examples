/*
* Copyright (C) 2013 - 2016  Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in this
* Software without prior written authorization from Xilinx.
*
*/

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gst/gst.h>

/* To turn on debug print messages, define DEBUG */
//#define DEBUG

#include "lab9-gst-state-example-menu.h"

/* Display Menu Function */
void showMenu (gstPipelineControl_t *overlayctl, 
    gstPipelineControl_t *tpgctl, gstPipelineControl_t *hdmictl)
{

    /* Display Menu Banner */
    fprintf (stdout, "%s", menuBanner);

    /* Show Pipeline Status Information */
    fprintf (stdout, "** Pipeline Status\n");
    fprintf (stdout, "%s", menuDivider);
    fprintf (stdout, "** %-15s | %-15s | %-15s | %s\n", "Pipeline", "Current State", "Target State", "Running Time");
    fprintf (stdout, "** %-15s | %-15s | %-15s | %"GST_TIME_FORMAT "\n", 
        overlayctl->description, gst_element_state_get_name (GST_STATE(overlayctl->pipeline)),
        gst_element_state_get_name (GST_STATE_TARGET(overlayctl->pipeline)), GST_TIME_ARGS(computeRunningTime(overlayctl)));
    fprintf (stdout, "** %-15s | %-15s | %-15s | %"GST_TIME_FORMAT "\n",
        tpgctl->description, gst_element_state_get_name (GST_STATE(tpgctl->pipeline)), 
        gst_element_state_get_name (GST_STATE_TARGET(tpgctl->pipeline)), GST_TIME_ARGS( computeRunningTime(tpgctl)));
    fprintf (stdout, "** %-15s | %-15s | %-15s | %"GST_TIME_FORMAT "\n", 
        hdmictl->description, gst_element_state_get_name (GST_STATE(hdmictl->pipeline)),
         gst_element_state_get_name (GST_STATE_TARGET(hdmictl->pipeline)), GST_TIME_ARGS( computeRunningTime(hdmictl)));
    fprintf (stdout, "%s", menuDivider);

    /* Show Time Overlay Pipeline Controls */
    fprintf (stdout, "** %-15s \n** - Options\n", overlayctl->description);
    fprintf (stdout, "**   ( ) No Options\n");
    fprintf (stdout, "%s", menuDivider);

    /* Show TPG Pipeline Controls */
    fprintf (stdout, "** %-15s \n** - Options\n", tpgctl->description);
    fprintf (stdout, "**   (1) Set pipeline %s to NULL    STATE\n", tpgctl->description);
    fprintf (stdout, "**   (2) Set pipeline %s to READY   STATE\n", tpgctl->description);
    fprintf (stdout, "**   (3) Set pipeline %s to PAUSED  STATE\n", tpgctl->description);
    fprintf (stdout, "**   (4) Set pipeline %s to PLAYING STATE\n", tpgctl->description);
    fprintf (stdout, "%s", menuDivider);

    /* Show HDMI Pipeline Controls */
    fprintf (stdout, "** %-15s \n** - Options\n", hdmictl->description);
    fprintf (stdout, "**   (5) Set pipeline %s to NULL    STATE\n", hdmictl->description);
    fprintf (stdout, "**   (6) Set pipeline %s to READY   STATE\n", hdmictl->description);
    fprintf (stdout, "**   (7) Set pipeline %s to PAUSED  STATE\n", hdmictl->description);
    fprintf (stdout, "**   (8) Set pipeline %s to PLAYING STATE\n", hdmictl->description);
    fprintf (stdout, "%s", menuDivider);

    /* Show program controls */
    fprintf (stdout, "** Program Control\n** - Options\n");
    fprintf (stdout, "**   (R) Refresh Pipeline Status\n");
    fprintf (stdout, "**   (T) Display Pipeline Time\n");
    fprintf (stdout, "**   (M) Display Pipeline Messages\n");
    fprintf (stdout, "**   (C) Clear the screen\n");
    fprintf (stdout, "**   (X) Exit the program\n");
    fprintf (stdout, "%s", menuDivider);

    fprintf (stdout, " Selection:> ");
}

/* Function to process input from console menu */
bool processInput(char *input, gstPipelineControl_t *overlayctl,
    gstPipelineControl_t *tpgctl, gstPipelineControl_t *hdmictl)
{
    bool ret = true;

    /* Menu Control Logic */
    switch (*input){
        /* TPG Pipeline Controls */
        case '1':
            tpgctl->sret = gst_element_set_state (tpgctl->pipeline, GST_STATE_NULL);
            fprintf (stdout, "%s: %s: Set state to NULL (Status: %s)\n", 
                prog, tpgctl->description, gst_element_state_change_return_get_name (tpgctl->sret));            
            break;

        case '2':
            tpgctl->sret = gst_element_set_state (tpgctl->pipeline, GST_STATE_READY);
            fprintf (stdout, "%s: %s: Set state to READY (Status: %s)\n", 
                prog, tpgctl->description, gst_element_state_change_return_get_name (tpgctl->sret));            
            break;

        case '3':
            tpgctl->sret = gst_element_set_state (tpgctl->pipeline, GST_STATE_PAUSED);
            fprintf (stdout, "%s: %s: Set state to PAUSED (Status: %s)\n", 
                prog, tpgctl->description, gst_element_state_change_return_get_name (tpgctl->sret));            
            break;

        case '4':
            tpgctl->sret = gst_element_set_state (tpgctl->pipeline, GST_STATE_PLAYING);
            fprintf (stdout, "%s: %s: Set state to PLAYING (Status: %s)\n", 
                prog, tpgctl->description, gst_element_state_change_return_get_name (tpgctl->sret));            
            break;

        /* HDMI Pipeline Controls */
        case '5':
            hdmictl->sret = gst_element_set_state (hdmictl->pipeline, GST_STATE_NULL);
            fprintf (stdout, "%s: %s: Set state to NULL (Status: %s)\n", 
                prog, hdmictl->description, gst_element_state_change_return_get_name (hdmictl->sret));            
            break;

        case '6':
            hdmictl->sret = gst_element_set_state (hdmictl->pipeline, GST_STATE_READY);
            fprintf (stdout, "%s: %s: Set state to READY (Status: %s)\n", 
                prog, hdmictl->description, gst_element_state_change_return_get_name (hdmictl->sret));            
            break;

        case '7':
            hdmictl->sret = gst_element_set_state (hdmictl->pipeline, GST_STATE_PAUSED);
            fprintf (stdout, "%s: %s: Set state to PAUSED (Status: %s)\n", 
                prog, hdmictl->description, gst_element_state_change_return_get_name (hdmictl->sret));            
            break;

        case '8':
            hdmictl->sret = gst_element_set_state (hdmictl->pipeline, GST_STATE_PLAYING);
            fprintf (stdout, "%s: %s: Set state to PLAYING (Status: %s)\n", 
                prog, hdmictl->description, gst_element_state_change_return_get_name (hdmictl->sret));            
            break;

        /* Program controls */
        case 'r': /* Refresh pipeline status */
        case 'R':
            debug_print ("%s: DEBUG: Refreshing pipeline status\n", prog);
            break;

        case 't': /* Display Pipeline Time Information */
        case 'T':
            debug_print ("%s: DEBUG: Display Pipeline Time Information\n", prog);
            /* Display Time Information for each pipeline */
            showTimes (overlayctl);
            showTimes (tpgctl);
            showTimes (hdmictl);
            break;

        case 'm': /* Display pipeline messages */
        case 'M': 
            debug_print ("%s: DEBUG: Display Pipeline Messages\n", prog);
            /* Display any messages on the bus */
            showMessages (overlayctl->bus, GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
                GST_MESSAGE_STATE_CHANGED, overlayctl->description);
            showMessages (tpgctl->bus, GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
                GST_MESSAGE_STATE_CHANGED, tpgctl->description);
            showMessages (hdmictl->bus, GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
                GST_MESSAGE_STATE_CHANGED, hdmictl->description);
            break;

        case 'c': /* Clear the console display */
        case 'C':
            /* clear the console */
            if( system (clearScreen) ) {
                fprintf (stderr, "%s: No shell available, aborting!\n", prog);
                ret = false;
            }
            break;

        case 'x': /* Exit program */
        case 'X':
            debug_print ("%s: DEBUG: Exit Program\n", prog);
            ret = false;
            break;

        default:
            debug_print ("%s: DEBUG: Invalid Menu Selection (%c)!\n", prog, *input);
            break;
    }
    return ret;
}
