/* LAB9 Gstreamer State Example Global Header File */

#ifndef LAB9GSTSTATEEXAMPLE_GLOBALS_H_
#define LAB9GSTSTATEEXAMPLE_GLOBALS_H_

/* Enable debug print messages in application */
#ifdef DEBUG
    #define debug_print(fmt, ...) fprintf(stderr, "%s:%s:%d: "fmt, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
    #define debug_print(fmt, ...)
#endif

/* Type Definitions */
typedef enum {
	NO_RXRESOLUTION, 
	RXRESOLUTION1080P, 
	RXRESOLUTION4K
} rxresolutiontype_t;

typedef struct _gstPipelineControl_t {
	GstElement *pipeline;
	GstBus *bus;
	GstMessage *msg;
	GError *err;
	GstClock *clock;
	GstState currentState;
	GstState pendingState;
	GstStateChangeReturn sret;
	const char *description;
} gstPipelineControl_t;

/* Constants */
extern const char *hdmiPipelineTest1080p;
extern const char *hdmiPipelineHDMIRx4k;
extern const char *hdmiPipelineHDMIRx1080p;
extern const char *menuBanner;
extern const char *menuDivider;
extern const char *clearScreen;

/* Global Variables */
extern const char *prog;
extern int rxresolution_1080p;
extern int rxresolution_4k;

/* Global Function Prototypes */
extern void showMenu (gstPipelineControl_t *overlayctl, gstPipelineControl_t *tpgctl, 
	gstPipelineControl_t *hdmictl);
extern bool processInput (char *input, gstPipelineControl_t *overlayctl, 
	gstPipelineControl_t *tpgctl, gstPipelineControl_t *hdmictl);
extern void showMessages (GstBus *bus, GstClockTime wait, GstMessageType types, 
	const char *description);
extern void showTimes (gstPipelineControl_t *ctl);
extern void showStateInfo (gstPipelineControl_t *ctl);
extern GstClockTime computeRunningTime (gstPipelineControl_t *ctl);
#endif