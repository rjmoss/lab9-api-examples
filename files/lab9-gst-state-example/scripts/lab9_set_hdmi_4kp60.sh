#!/bin/sh

# v2020.1 VCU TRD Multistream design

# Set the HDMI-TX display to 4kp60
modetest -D a0070000.v_mix -s 44:3840x2160-60@BG24 
