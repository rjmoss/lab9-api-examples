#!/bin/sh

# v2020.1 VCU TRD Multistream design

# Set the kernel log level to 3 (ALERT, CRITICAL, ERROR)
dmesg -n 3

# Initialize NVIDIA Shield for 1080p60 native content
media-ctl -d /dev/media8 -V "\"a0080000.v_proc_ss\":0 [fmt:RBG888_1X24/1920x1080 field:none]"
media-ctl -d /dev/media8 -V "\"a0080000.v_proc_ss\":1 [fmt:VYYUYY8_1X24/1920x1080 field:none]" 
v4l2-ctl -d /dev/video0  --set-fmt-video width=1920,height=1080,pixelformat='NV12'

# Show the HDMI-RX topology
media-ctl -d /dev/media8 -p

# Show the V4L2 Capture Format
v4l2-ctl -d /dev/video0 -V