# HDMI-TX Plane Configuration for lab9-gst-state-example
- Based on the VCU TRD Multistream 2020.1 design

## Display Plane Configuration

### DRM Topology
- Connector:  44
- Encoder:    43
- CRTC:     42
  - Planes: 
    - 33 (overlay, NV12) - HDMI Pass Through
    - 34 (overlay, NV12) - Software TPG (SMPTE colorbars)
    - 35 (overlay, NV12) - 
    - 36 (overlay, NV12) - 
    - 37 (overlay, RG24) - 
    - 38 (overlay, RG24) - 
    - 39 (overlay, NV12) - 
    - 40 (overlay, AR24) - Timer overlay (black background)
    - 41 (primary, BG24)

### Text/Time Overlay

```
   [0,0]|----------------------[1920,0   ]----------------------|[3840,0   ]
    [ 100,100]---------[ 700, 100]  |                           |
        |    |   TIME  |            |                           |
    [ 100,200]---------[ 700, 200]  |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
[0,1080]|----------------------[1920,1080]----------------------||[3840,1080]
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
[0,2160]|----------------------[1920,2160]----------------------|[3840,2160]
```

### Software TPG
- 1 x 720p30 software test pattern (1280x720) (SMPTE colobars)

```
   [0,0]|----------------------[1920,0   ]----------------------|[3840,0   ]
        |                           |                           |
        |                      [2240, 180]|---------------|[3520, 180]|
        |                           |     |               |     |
        |                           |     |    SMPTE      |     |
        |                           |     |  1280x720@30  |     |
        |                           |     |               |     |
        |                      [2240, 900]|---------------|[3520, 900]|
[0,1080]|---------------------------|---------------------------||[3840,1080]
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
[0,2160]|----------------------[1920,2160]----------------------|[3840,2160]
```

### HDMI-RX

#### Configuration #1 - 4K Pass through
```
   [0,0]|----------------------[1920,0   ]----------------------|[3840,0   ]
        |                                                       |
        |                                                       |
        |                                                       |
        |                                                       |
        |                                                       |
        |                                                       |
        |                     Full screen                       |
        |                    HDMI Pass Through                  |
        |                                                       |
        |                                                       |
        |                                                       |
        |                                                       |
        |                                                       |
        |                                                       |
        |                                                       |
[0,2160]|----------------------[1920,2160]----------------------|[3840,2160]
```

#### Configuration #2 - 1080p Pass through
```
   [0,0]|----------------------[1920,0   ]----------------------|[3840,0   ]
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
        |                           |                           |
[0,1080]|----------------------[1920,1080]----------------------||[3840,1080]
        |                           |                           |
        |                           |                           |
        |                           |       Lower Quadrant      |
        |                           |     HDMI Pass Through     |
        |                           |                           |
        |                           |                           |
        |                           |                           |
[0,2160]|----------------------[1920,2160]----------------------|[3840,2160]
```

## Set HDMI-TX Resolution to 4K UHD 60Hz
- Match the framerate of the lowest framerate source (Sintel Trailer, Caminandes)

```bash
modetest -D a0070000.v_mix -s 44:3840x2160-60@BG24
```

## Generage Time Overlay

gst-launch-1.0 -v videotestsrc pattern=2 ! \
video/x-raw,width=600,height=100,framerate=30/1 ! \
timeoverlay \
halignment=left valignment=top text="Buffer Time:" \
shaded-background=true shading-value=128 font-desc="Sans,36" ! \
queue ! \
kmssink bus-id="a0070000.v_mix" plane-id=40 \
render-rectangle="<100,100,600,100>" \
async=true &

## Generate SMPTE Colobar Test Pattern

gst-launch-1.0 -v videotestsrc pattern=0 ! \
video/x-raw,width=1280,height=720,framerate=30/1 ! \
queue ! \
kmssink bus-id="a0070000.v_mix" plane-id=34 \
render-rectangle="<2240,180,1280,720>" \
async=true &

## HDMI Pass Through

### Configuration #1 - 4K HDMI-RX Source

gst-launch-1.0 v4l2src device=/dev/video0 io-mode=4 \
! video/x-raw, width=3840, height=2160, framerate=60/1, format=NV12 \
! queue max-size-bytes=0 \
! kmssink bus-id="a0070000.v_mix" plane-id=33 &

### Configuration #2 - 1080p HDMI-RX Source

gst-launch-1.0 v4l2src device=/dev/video0 io-mode=4 \
! video/x-raw, width=1920, height=1080, framerate=60/1, format=NV12 \
! queue max-size-bytes=0 \
! kmssink bus-id="a0070000.v_mix" plane-id=33 \
render-rectangle="<1920,1080,1920,1080>" &
