/* LAB9 GStreamer State Control Example Menu Header File */

#ifndef LAB9GSTSTATEEXAMPLE_MENU_H_
#define LAB9GSTSTATEEXAMPLE_MENU_H_

#include <stdbool.h>
#include <gst/gst.h>
#include "globals.h"

/* Function Prototypes */
void showMenu (gstPipelineControl_t *overlayctl, gstPipelineControl_t *tpgctl, 
	gstPipelineControl_t *hdmictl);
bool processInput (char *input, gstPipelineControl_t *overlayctl, 
	gstPipelineControl_t *tpgctl, gstPipelineControl_t *hdmictl);

/* Program Constants */
const char *menuBanner = "\n\n"
"***************************************************\n"\
"** VCU Relaunch Lab #9                           **\n"\
"** GStreamer Pipeline State Example              **\n"\
"***************************************************\n";

const char *menuDivider = ""
"***************************************************\n";

const char *clearScreen = "clear";

#endif