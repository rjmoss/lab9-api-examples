/* LAB9 GStreamer State Example Main Header File */

#ifndef LAB9GSTSTATEEXAMPLE_H_
#define LAB9GSTSTATEEXAMPLE_H_

#include <stdbool.h>
#include <gst/gst.h>
#include "globals.h"

/* Function Prototypes */
void usage (const char *argv0);
bool initPipeline (gstPipelineControl_t *ctl, const char *pipeline);
void deinitPipeline (gstPipelineControl_t *ctl);
void showTargetState (GstState *targetState);
void showCurrentState (GstState *currentState);
void showNextState (GstState *nextState);
void showPendingState(GstState *pendingState);
void showChangeStatus (GstStateChangeReturn *sret);
void showTimes (gstPipelineControl_t *ctl);
void showStateInfo (gstPipelineControl_t *ctl);
void showMessages (GstBus *bus, GstClockTime wait, GstMessageType types, 
	const char *description);

/* Static Pipeline Definitions */
const char *timeOverlayPipelineDescription = ""\
"Time Overlay";

const char *timeOverlayPipeline = "videotestsrc pattern=2 "\
"! video/x-raw,width=600,height=100,framerate=30/1 "\
"! timeoverlay "\
" halignment=left valignment=top text=\"Running Time:\""\
" shaded-background=true shading-value=128 font-desc=\"Sans,36\" "\
"! queue "\
"! kmssink bus-id=\"a0070000.v_mix\" plane-id=40 "\
" render-rectangle=\"<100,100,600,100>\" sync=false";

const char *tpgPipelineDescription = ""\
"Soft TPG";

const char *tpgPipeline720p = "videotestsrc pattern=0 "\
"! video/x-raw, width=1280, height=720, framerate=30/1 "\
"! queue "\
"! kmssink bus-id=\"a0070000.v_mix\" plane-id=34 "\
" render-rectangle=\"<2240,180,1280,720>\" sync=false";

const char *hdmiPipelineHDMIRxDescription = ""\
"HDMI Input";

const char *hdmiPipelineHDMIRx4k = "v4l2src device=/dev/video0 io-mode=4 "\
"! video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 "\
"! queue max-size-bytes=0 "\
"! kmssink bus-id=\"a0070000.v_mix\" plane-id=33";

const char *hdmiPipelineHDMIRx1080p = "v4l2src device=/dev/video0 io-mode=4 "\
"! video/x-raw,format=NV12,width=1920,height=1080,framerate=60/1 "\
"! queue max-size-bytes=0 "\
"! kmssink bus-id=\"a0070000.v_mix\" plane-id=33 "\
" render-rectangle=\"<1920,1080,1920,1080>\"";

#endif