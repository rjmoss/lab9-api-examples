/* LAB9 Gstreamer Dynamic TPG Example Menu Header File */

#ifndef LAB9DYNAMICTPGEXAMPLE_MENU_H_
#define LAB9DYNAMICTPGEXAMPLE_MENU_H_

#include <stdbool.h>
#include <gst/gst.h>
#include "globals.h"

/* Function Prototypes */
void showMenu (gstPipelineControl_t *pctl, int level);
bool processInput (char *input, gstPipelineControl_t *pctl, int *level);

/* Program Constants */
const char *menuBanner = "\n\n"
"***************************************************\n"\
"** VCU Relaunch Lab #9                           **\n"\
"** GStreamer Dynamic TPG Example                 **\n"\
"***************************************************\n";

const char *menuDivider = ""
"***************************************************\n";

const char *clearScreen = "clear";

#endif