/* LAB9 Gstreamer Dynamic TPG Example Main Header File */

#ifndef LAB9DYNAMICTPGEXAMPLE_H_
#define LAB9DYNAMICTPGEXAMPLE_H_

#include <stdbool.h>
#include <gst/gst.h>
#include "globals.h"

/* Function Prototypes */
void usage (const char *argv0);
bool initPipeline (gstPipelineControl_t *ctl);
void deinitPipeline (gstPipelineControl_t *ctl);
void showTargetState (GstState *targetState);
void showCurrentState (GstState *currentState);
void showNextState (GstState *nextState);
void showPendingState(GstState *pendingState);
void showChangeStatus (GstStateChangeReturn *sret);
void showTimes (gstPipelineControl_t *ctl);
void showStateInfo (gstPipelineControl_t *ctl);
void showMessages (GstBus *bus, GstClockTime wait, GstMessageType types, 
	const char *description);
void changePattern (gstPipelineControl_t *ctl, int newpattern);

/* Static Pipeline Definitions */
const char *pipelineDescription = ""\
"Dynamic TPG";

const char *drmBusIDHDMI= "a0070000.v_mix";

const char *drmBusIDDP = "fd4a0000.zynqmp-display";

#endif