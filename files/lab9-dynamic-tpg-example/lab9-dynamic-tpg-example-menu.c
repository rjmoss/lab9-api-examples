/*
* Copyright (C) 2013 - 2016  Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in this
* Software without prior written authorization from Xilinx.
*
*/

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gst/gst.h>

/* To turn on debug print messages, define DEBUG */
//#define DEBUG

#include "lab9-dynamic-tpg-example-menu.h"

/* Globals */
int menuLevel = 0;

/* Display Menu Function */
void showMenu (gstPipelineControl_t *pctl, int level)
{

    /* Display Menu Banner */
    fprintf (stdout, "%s", menuBanner);

    /* Show Pipeline Status Information */
    fprintf (stdout, "** Pipeline Status\n");
    fprintf (stdout, "%s", menuDivider);
    fprintf (stdout, "** %-15s | %-15s | %-15s | %s\n", "Pipeline", "Current State", "Target State", "Running Time");
    fprintf (stdout, "** %-15s | %-15s | %-15s | %"GST_TIME_FORMAT "\n", 
        pctl->description, gst_element_state_get_name (GST_STATE(pctl->pipeline)),
        gst_element_state_get_name (GST_STATE_TARGET(pctl->pipeline)), GST_TIME_ARGS(computeRunningTime(pctl)));
    fprintf (stdout, "%s", menuDivider);

    /* Show different sub-menu based on level */
    if (level == 1){
        /* Show Test Pattern Generator Options */
        fprintf (stdout, "** %-15s \n** - Options\n", gst_element_get_name (pctl->source));
        fprintf (stdout, "**   (0 .. 24) Select the %s test pattern\n", pctl->description);
        fprintf (stdout, "%s", menuDivider);
        /* Show program controls */
        fprintf (stdout, "** Program Control\n** - Options\n");
        fprintf (stdout, "**   (X) Exit the main menu\n");
    } else {
        /* Show Pipeline Controls */
        fprintf (stdout, "** %-15s \n** - Options\n", pctl->description);
        fprintf (stdout, "**   (1) Set pipeline %s to NULL    STATE\n", pctl->description);
        fprintf (stdout, "**   (2) Set pipeline %s to READY   STATE\n", pctl->description);
        fprintf (stdout, "**   (3) Set pipeline %s to PAUSED  STATE\n", pctl->description);
        fprintf (stdout, "**   (4) Set pipeline %s to PLAYING STATE\n", pctl->description);
        fprintf (stdout, "%s", menuDivider);

        /* Show Parameter Controls */
        fprintf (stdout, "** %-15s \n** - Options\n", "Test Pattern");
        fprintf (stdout, "**   (P) Display Test Pattern Sub-menu \n");
        fprintf (stdout, "%s", menuDivider);

        /* Show program controls */
        fprintf (stdout, "** Program Control\n** - Options\n");
        fprintf (stdout, "**   (R) Refresh Pipeline Status\n");
        fprintf (stdout, "**   (T) Display Pipeline Time\n");
        fprintf (stdout, "**   (M) Display Pipeline Messages\n");
        fprintf (stdout, "**   (C) Clear the screen\n");
        fprintf (stdout, "**   (X) Exit the program\n");
    }
    fprintf (stdout, "%s", menuDivider);
    fprintf (stdout, " Selection:> ");
}

/* Function to process input from console menu */
bool processInput(char *input, gstPipelineControl_t *pctl, int *level)
{
    bool ret = true;
    int nextPattern = 0;
    debug_print ("%s: processInput (%c) (%d) level=%i\n", prog, *input, *input, *level);
    if (*level == 1) {
        debug_print ("%s: processInput menu level=%i\n", prog, *level);
        /* Test Pattern Submenu */
        switch (*input){
            /* Exit to main menu */
            case 'x':
            case 'X':
                fprintf (stderr, "%s: Exiting to main menu\n", prog);
                *level = *level - 1;
                break;

            /* Process test pattern number */
            default:
                nextPattern = atoi (input);
                debug_print ("%s: input=(%s), nextPattern=(%i)\n", prog, input, nextPattern);
                if ((nextPattern < LAB9_MIN_TESTPATTERN) || (nextPattern > LAB9_MAX_TESTPATTERN)){
                    fprintf (stderr, "%s: Invalid test pattern value (%i)\n", prog, nextPattern);
                } else {
                    fprintf (stdout, "%s: Changing test pattern to %i\n", prog, nextPattern);
                    //Change test pattern here
                    changePattern (pctl, nextPattern);
                    *level = *level - 1;
                }                   
                break;
        }
    } else {
    /* Main Menu Control Logic */
        debug_print ("%s: processInput menu level=%i\n", prog, *level);
        switch (*input){
            /* Pipeline Controls */
            case '1':
                pctl->sret = gst_element_set_state (pctl->pipeline, GST_STATE_NULL);
                fprintf (stdout, "%s: %s: Set state to NULL (Status: %s)\n", 
                    prog, pctl->description, gst_element_state_change_return_get_name (pctl->sret));            
                break;

            case '2':
                pctl->sret = gst_element_set_state (pctl->pipeline, GST_STATE_READY);
                fprintf (stdout, "%s: %s: Set state to READY (Status: %s)\n", 
                    prog, pctl->description, gst_element_state_change_return_get_name (pctl->sret));            
                break;

            case '3':
                pctl->sret = gst_element_set_state (pctl->pipeline, GST_STATE_PAUSED);
                fprintf (stdout, "%s: %s: Set state to PAUSED (Status: %s)\n", 
                    prog, pctl->description, gst_element_state_change_return_get_name (pctl->sret));            
                break;

            case '4':
                pctl->sret = gst_element_set_state (pctl->pipeline, GST_STATE_PLAYING);
                fprintf (stdout, "%s: %s: Set state to PLAYING (Status: %s)\n", 
                    prog, pctl->description, gst_element_state_change_return_get_name (pctl->sret));            
                break;

            /* Test Pattern Options */
            case 'p':
            case 'P':
                *level = *level+1;
                fprintf (stdout, "%s: %s: Selected Sub Menu (Level: %i)\n", prog, pctl->description, *level);            
                break;

            /* Program controls */
            case 'r': /* Refresh pipeline status */
            case 'R':
                debug_print ("%s: DEBUG: Refreshing pipeline status\n", prog);
                break;

            case 't': /* Display Pipeline Time Information */
            case 'T':
                debug_print ("%s: DEBUG: Display Pipeline Time Information\n", prog);
                /* Display Time Information for each pipeline */
                showTimes (pctl);
                break;

            case 'm': /* Display pipeline messages */
            case 'M': 
                debug_print ("%s: DEBUG: Display Pipeline Messages\n", prog);
                /* Display any messages on the bus */
                showMessages (pctl->bus, GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
                    GST_MESSAGE_STATE_CHANGED, pctl->description);
                break;

            case 'c': /* Clear the console display */
            case 'C':
                /* clear the console */
                if( system (clearScreen) ) {
                    fprintf (stderr, "%s: No shell available, aborting!\n", prog);
                    ret = false;
                }
                break;

            case 'x': /* Exit program */
            case 'X':
                debug_print ("%s: DEBUG: Exit Program\n", prog);
                ret = false;
                break;

            default:
                fprintf ("%s: DEBUG: Invalid Menu Selection (%c) (%d)\n", prog, *input, *input);
                debug_print ("%s: DEBUG: Invalid Menu Selection (%c)!\n", prog, *input);
                break;
        }
    }
    fprintf (stdout, "%s: processInput return\n", prog);
    return ret;
}
