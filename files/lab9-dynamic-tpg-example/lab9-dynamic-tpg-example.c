/*
* Copyright (C) 2013 - 2016  Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in this
* Software without prior written authorization from Xilinx.
*
*/

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>

#include <gst/gst.h>

/* To turn on debug print messages, define DEBUG */
//#define DEBUG

#include "lab9-dynamic-tpg-example.h"

/* Flags set by '--display-dp' and '--display-hdmi' options */
int display_dp = 0;
int display_hdmi = 0;

/* Flags set by '--720p', --1080p' and '--4k' options */
int resolution_720p = 0;
int resolution_1080p = 0;
int resolution_4k = 0;

/* Program Name */
const char *prog;

/* Command line long option formatting */
static struct option long_options[] = 
{
    {"display-dp", no_argument, &display_dp, 1},
    {"display-hdmi", no_argument, &display_hdmi, 1},
    {"720p", no_argument, &resolution_720p, 1},
    {"720P", no_argument, &resolution_720p, 1},
    {"1080p", no_argument, &resolution_1080p, 1},
    {"1080P", no_argument, &resolution_1080p, 1},
    {"4k", no_argument, &resolution_4k, 1},
    {"4K", no_argument, &resolution_4k, 1},
    {"pattern", required_argument, NULL, 'p'},
    {"help", no_argument, NULL, 'h'},
    {NULL, 0, NULL, 0}
};

int main (int argc, char **argv)
{
	int opt, optcnt = 0;
    const char *displayselect;
    char *p_optarg;

    prog = argv[0];

    /* Selected Default Display Type */
    displaytype_t displaytype = NO_DISPLAY;

    /* Select Default Resolution */
    resolutiontype_t resolution = NO_RESOLUTION;

    /* test pattern */
    unsigned int test_pattern = 0;

    fprintf (stdout, "%s: GStreamer Dynamic TPG Example - VCU Relaunch Session #9\n", prog);

    /* Parse command line options */
    while ((opt = getopt_long (argc, argv, "hp:", long_options, NULL)) != -1) {
        optcnt++;
    	switch (opt) {
        case 'h': /* Command Help */
            usage (argv[0]);
            exit (EXIT_SUCCESS);
            break;

        case 'p': /* Select starting test pattern */
            p_optarg = optarg;
            test_pattern = atoi(p_optarg);
            if ( (test_pattern < LAB9_MIN_TESTPATTERN) || 
                (test_pattern > LAB9_MAX_TESTPATTERN)){
                fprintf (stdout, "%s: Invalid Test Pattern Value (%u) - Must be in the range of [%u .. %u]\n", 
                    prog, test_pattern, LAB9_MIN_TESTPATTERN, LAB9_MAX_TESTPATTERN);
                exit (EXIT_FAILURE);
            } else {
                fprintf (stdout, "%s: Selected Test Pattern (%u)\n", prog, test_pattern);
            }
            break;

        default: /* Long Options */
            debug_print ("%s: DEBUG: Long Option=[%c]\n", prog, opt);
            if (display_dp){
                debug_print ("%s: DEBUG: Long option --display-dp set\n", prog);
                displaytype = DISPLAYPORT;
                display_dp = 0;
            } else if (display_hdmi){
                debug_print ("%s: DEBUG: Long option --display-hdmi set\n", prog);
                displaytype = HDMI;
                display_hdmi = 0;
            } else if (resolution_720p){
                debug_print ("%s: DEBUG: Long option --resolution_720p set\n", prog);
                resolution = RESOLUTION720P;
                resolution_1080p = 0;
            } else if (resolution_1080p){
                debug_print ("%s: DEBUG: Long option --resolution_1080p set\n", prog);
                resolution = RESOLUTION1080P;
                resolution_1080p = 0;
            } else if (resolution_4k){
                debug_print ("%s: DEBUG: Long option --resolution_4k set\n", prog);
                resolution = RESOLUTION4K;
                resolution_4k = 0;
            } else {
                debug_print ("%s: DEBUG: Invalid option (%c)\n", prog, opt);
                usage (argv[0]);
                exit (EXIT_FAILURE);
            }
            break;
        }
    }

    /* If not options specified, show usage and exit */
    if (optcnt == 0) {
        usage (argv[0]);
        exit (EXIT_SUCCESS);
    }

    /* Check that a resolution and display interface have been specified */
    if ((displaytype == NO_DISPLAY) || (resolution == NO_RESOLUTION)){
        fprintf (stderr, "%s: Please select a display interface and resolution\n", prog);
        usage (argv[0]);
        exit (EXIT_FAILURE);
    }

    outputFormat_t displayformat;

    /* Select the output pipeline based on the display and source configuration */
    switch (displaytype) {

    case DISPLAYPORT:
        debug_print ("%s: DEBUG: DisplayPort selected: Fixed ID = fd4a0000.zynqmp-display\n", prog);
        switch (resolution) {
        case RESOLUTION720P:
            debug_print ("%s: DEBUG: DisplayPort+720p selected\n", prog);
            displayselect = drmBusIDDP;
            displayformat.width = 1280;
            displayformat.height = 720;
            displayformat.framerate = 16;
            break;

        case RESOLUTION1080P:
            debug_print ("%s: DEBUG: DisplayPort+1080p selected\n", prog);
            displayselect = drmBusIDDP;
            displayformat.width = 1920;
            displayformat.height = 1080;
            displayformat.framerate = 16;
            break;

        case RESOLUTION4K:
            debug_print ("%s: DEBUG: DisplayPort+4K selected\n", prog);
            displayselect = drmBusIDDP;
            displayformat.width = 3840;
            displayformat.height = 2160;
            displayformat.framerate = 4;
            break;

        case NO_RESOLUTION:
        default:
            debug_print ("%s: DEBUG: Undefined resolution!\n", prog);
            usage (argv[0]);
            exit (EXIT_FAILURE);
        break;
        }
        break;

    case HDMI:
        debug_print ("%s: DEBUG: HDMI selected: Fixed ID = a0070000.v_mix\n", prog);
        switch (resolution) {
        case RESOLUTION720P:
            debug_print ("%s: DEBUG: HDMI+720p selected\n", prog);
            displayselect = drmBusIDHDMI;
            displayformat.width = 1280;
            displayformat.height = 720;
            displayformat.framerate = 16;
            break;

        case RESOLUTION1080P:
            debug_print ("%s: DEBUG: HDMI+1080p selected\n", prog);
            displayselect = drmBusIDHDMI;
            displayformat.width = 1920;
            displayformat.height = 1080;
            displayformat.framerate = 16;
            break;

        case RESOLUTION4K:
            debug_print ("%s: DEBUG: HDMI+4K selected\n", prog);
            displayselect = drmBusIDHDMI;
            displayformat.width = 3840;
            displayformat.height = 2160;
            displayformat.framerate = 4;
            break;
        
        case NO_RESOLUTION:
        default:
            debug_print ("%s: DEBUG: Undefined resolution!\n", prog);
            usage (argv[0]);
            exit (EXIT_FAILURE);
            break;
        }
        break;

    case NO_DISPLAY:
    default:
        debug_print ("%s: DEBUG: Undefined display type!\n", prog);
        usage (argv[0]);
        exit (EXIT_FAILURE);
    }

    /* Initialize GStreamer */    
    gst_init (NULL, NULL);

    gstPipelineControl_t pctl;

    /* Create the pipeline elements */
    /* videotestsrc ! tpg_cap -> queue -> kmssink */
    pctl.source = gst_element_factory_make ("videotestsrc", "source");
    pctl.capsfilter = gst_element_factory_make ("capsfilter", "capsfilter");
    pctl.queue = gst_element_factory_make ("queue", "queue");
    pctl.sink = gst_element_factory_make ("kmssink", "sink");

    /* Create an empty pipeline */
    pctl.pipeline = gst_pipeline_new ("dynamic-tpg-pipeline");

    /* Validate all elements in the pipeline were created */
    if (!pctl.pipeline || !pctl.source || !pctl.capsfilter || !pctl.queue || !pctl.sink){
        fprintf (stdout, "%s: Pipeline element creation failed.\n", prog);
        exit (EXIT_FAILURE);
    }

    /* Add the elements to the pipeline */
    gst_bin_add_many (GST_BIN (pctl.pipeline), pctl.source, pctl.capsfilter, 
        pctl.queue, pctl.sink, NULL);

    /* Set the test source format using caps filters */
    pctl.tpg_caps = gst_caps_new_simple ("video/x-raw",
        "width", G_TYPE_INT, displayformat.width,
        "height", G_TYPE_INT, displayformat.height,
        "framerate", GST_TYPE_FRACTION, displayformat.framerate, 1,
        NULL);

    fprintf (stdout, "%s: Using Caps %"GST_PTR_FORMAT"\n", prog, pctl.tpg_caps);

    /* Establish links between elements in the pipeline */
    
    /* Set the test pattern */
    g_object_set (pctl.source, "pattern", test_pattern, NULL);

    /* #1: Link the videotestsrc element to capsfilter element */
    if (!gst_element_link_filtered (pctl.source, pctl.capsfilter, pctl.tpg_caps)){
        fprintf (stderr, "%s: Error linking %s with %s\n", prog, 
            gst_element_get_name (pctl.source), gst_element_get_name (pctl.capsfilter));
    }
    gst_caps_unref (pctl.tpg_caps);

    /* #2: Link the capsfilter to the queue element */
    if (!gst_element_link (pctl.capsfilter, pctl.queue)){
        fprintf (stderr, "%s: Error linking %s with %s\n", prog, 
            gst_element_get_name (pctl.capsfilter), gst_element_get_name (pctl.queue));
    }
    
    /* Set the drm bus ID pattern */
    debug_print ("%s: Setting DRM Bus ID to (%s)\n", prog, displayselect);

    g_object_set (pctl.sink, "bus-id", displayselect, NULL);
    /* Set the fullscreen overlay property */
    g_object_set (pctl.sink, "fullscreen-overlay", true, NULL);

    /* #3: Link the queue to kmssink */
    if (!gst_element_link (pctl.queue, pctl.sink)){
        fprintf (stderr, "%s: Error linking %s with %s\n", prog, 
            gst_element_get_name (pctl.queue), gst_element_get_name (pctl.sink));
    }


    /***************************************************************/
    /* Initialize GStreamer Pipeline created above                 */
    /***************************************************************/
    pctl.description = pipelineDescription;

    if ( !initPipeline (&pctl) ){
        exit (EXIT_FAILURE);
    }

    /* Display any messages on the bus */
    showMessages (pctl.bus, GST_SECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED, pctl.description);

    /**************************/
    /* Start the Pipeline     */
    /**************************/
    /* Start the pipelines */
    gst_element_set_state (pctl.pipeline, GST_STATE_PLAYING);
    /* Check bus for messages */
    showMessages (pctl.bus, 100*GST_MSECOND, GST_MESSAGE_ERROR | GST_MESSAGE_EOS | 
        GST_MESSAGE_STATE_CHANGED, pctl.description);

    char input;
    int nextPattern;
    bool exit_loop = false;
    char inputBuffer[MAX_INPUT_BUFFER]; /* max should be a 2 digit value for test pattern input*/

    do {
        showMenu (&pctl, menuLevel);
        switch (menuLevel){
            case 1: /* Test Pattern Selection Menu */
                if (fgets (inputBuffer, sizeof(inputBuffer), stdin) != NULL){
                    inputBuffer[strlen(inputBuffer) - 1] = '\0';
                    exit_loop = !processInput (&inputBuffer[0], &pctl, &menuLevel);
                } else {
                    fprintf (stderr, "%s: Invalid pattern value detected\n", prog);
                    inputBuffer[0] = '\0';                        
                }
                break;

            case 0: /* Main pipeline control menu */
            default:
                if (fgets (inputBuffer, sizeof(inputBuffer), stdin) != NULL){
                    inputBuffer[strlen(inputBuffer) - 1] = '\0';
                    exit_loop = !processInput (&inputBuffer[0], &pctl, &menuLevel);
                } else {
                    inputBuffer[0] = '\0';
                }
                //else
                // {
                //     /* Flush the console input buffer */
                //     while ((input = getchar ()) != EOF && (input != '\n'));
                // }
                break;
        }        

        sleep(0);

    } while ( !exit_loop );

    /* Pause any running pipelines */

    if (GST_STATE(pctl.pipeline) == GST_STATE_PLAYING){
        pctl.sret = gst_element_set_state (pctl.pipeline, GST_STATE_PAUSED);
        /* Wait for te state change to take affect by listening to the message bus */
        fprintf (stdout, "%s: %s: State Change Return Status (%s)\n", prog, pctl.description, 
            gst_element_state_change_return_get_name(pctl.sret));
    }

    /* Check the message bus for a state change message */
    showMessages (pctl.bus, 100*GST_MSECOND, 
        GST_MESSAGE_EOS | GST_MESSAGE_STATE_CHANGED, pctl.description);

    /* Show Final Status and Runtime Information */
    fprintf (stdout, "%s", menuDivider);
    fprintf (stdout, "** Final Pipeline Status\n");
    fprintf (stdout, "** %-15s | %-15s | %s\n", "Pipeline", "Current State", "Running Time");
    fprintf (stdout, "** %-15s | %-15s | %"GST_TIME_FORMAT "\n", 
        pctl.description, gst_element_state_get_name (GST_STATE(pctl.pipeline)), 
        GST_TIME_ARGS( computeRunningTime (&pctl)));
    fprintf (stdout, "%s", menuDivider);

    /**************************/
    /* Stop the Pipeline      */
    /**************************/
    /* Stop Pipelines */
    debug_print ("%s: %s: DEBUG: Stoppping Pipeline\n", prog, pctl.description);
    gst_element_set_state (pctl.pipeline, GST_STATE_NULL);

    /***************************************************************/
    /* Free GStreamer Pipeline Resources for the HDMI Pass Through */
    /***************************************************************/
    deinitPipeline (&pctl);

    exit (EXIT_SUCCESS);
}

/* Command line usage message */
void usage (const char *argv0)
{
    fprintf (stderr, "Display a software generated test pattern on the specified display\n");
    fprintf (stderr, "Usage: %s [display] [resolution] [options]\n", argv0);
    fprintf (stderr, " Available [display] options:\n");
    fprintf (stderr, "\t --display-dp          Send test pattern to PS DisplayPort\n");
    fprintf (stderr, "\t --display-hdmi        Send test pattern to HDMI-TX through the Video Mixer\n");
    fprintf (stderr, " Available [resolution] options:\n");
    fprintf (stderr, "\t --720p,  --720P       Use a 720p software generated test pattern\n");    
    fprintf (stderr, "\t --1080p, --1080P      Use a 1080p software generated test pattern\n");
    fprintf (stderr, "\t --4k, --4K            Use a 4K software generated test pattern\n");
    fprintf (stderr, "Available [options]:\n");
    fprintf (stderr, "\t -p, --pattern <0 .. 24>     Select a videotestsrc pattern\n");
    fprintf (stderr, "\t -h, --help                  Print this help screen and exit\n"); 
}

/* Initialize Pipeline */
//bool initPipeline (gstPipelineControl_t *ctl, const char *pipeline)
bool initPipeline (gstPipelineControl_t *ctl)
{
    bool ret = true;
    ctl->err = NULL;
    
    if (!ctl->pipeline) {
        fprintf (stderr, "%s: %s: Error in pipeline creation [%s]\n", 
            prog, ctl->description, ctl->err->message);
        ret = false;
    } else if (ctl->err != NULL){
        fprintf (stderr, "%s: %s: Error message set creating pipeline [%s]\n", 
            prog, ctl->description, ctl->err->message);
        g_error_free (ctl->err);
        ret = false;
    } else {

        ctl->bus = NULL;

        /* Get the pipeline bus reference and Clock */
        ctl->bus = gst_element_get_bus (ctl->pipeline);

        /* Get Initial State */
        debug_print ("%s: %s: DEBUG: Getting initial state after creation\n", 
            prog, ctl->description);
        ctl->sret = gst_element_get_state (ctl->pipeline, &ctl->currentState, 
            &ctl->pendingState, GST_CLOCK_TIME_NONE);

        /* Get the pipeline bus reference and Clock */
        ctl->bus = gst_element_get_bus (ctl->pipeline);
        debug_print ("%s: %s: DEBUG: getting clock\n", prog, ctl->description);
        ctl->clock = gst_element_get_clock (ctl->pipeline);
    }
    return ret;
}

/* De-Initialize Pipeline */
void deinitPipeline (gstPipelineControl_t *ctl)
{
    if (ctl->clock != NULL){
        debug_print ("%s: %s: Unref Clock\n", prog, ctl->description);
        gst_object_unref (ctl->clock);
    }
    if (ctl->err != NULL) {
        debug_print ("%s: %s: Error message set parsing pipeline [%s]\n", 
            prog, ctl->description, ctl->err->message);
        g_error_free (ctl->err);
    }
    if (ctl->msg != NULL) {
        debug_print ("%s: %s: Unref Msg\n", prog, ctl->description);
        showMessages (ctl->bus, GST_MSECOND, GST_MESSAGE_ANY, ctl->description);
    }
    if (ctl->bus != NULL) {
        debug_print ("%s: %s: Unref Bus\n", prog, ctl->description);
        gst_object_unref (ctl->bus);
    }
    if (ctl->pipeline != NULL) {
        debug_print ("%s: %s: Unref Pipeline\n", prog, ctl->description);
        gst_object_unref (ctl->pipeline);
    }   
}

/* Change the active test pattern */
void changePattern (gstPipelineControl_t *ctl, int newpattern)
{
    /* Set the test pattern */
    g_object_set (ctl->source, "pattern", newpattern, NULL);
}

/* Compute running time */
GstClockTime computeRunningTime (gstPipelineControl_t *ctl)
{
    /* Refresh state information */
    ctl->sret = gst_element_get_state (ctl->pipeline, &ctl->currentState, 
        &ctl->pendingState, GST_CLOCK_TIME_NONE);
    GstClockTime runtime = 0;   

    switch (ctl->currentState) {
        case GST_STATE_PLAYING:
            debug_print ("%s: %s: DEBUG: Computing running time for PLAYING pipeline\n", 
                prog, ctl->description );
            runtime = (gst_clock_get_time (ctl->pipeline->clock) - 
                gst_element_get_base_time (ctl->pipeline)); 
            break;

        /*case GST_STATE_VOID_PENDING:
        case GST_STATE_NULL:
        case GST_STATE_READY:
        case GST_STATE_PAUSED: */
        default:
            debug_print ("%s: %s: DEBUG: Computing running time for NON-PLAYING pipeline\n", 
                prog, ctl->description );
            runtime = gst_element_get_start_time(ctl->pipeline);
            break;
    }
    return runtime;
}

/* Show pipeline state information */
void showStateInfo (gstPipelineControl_t *ctl)
{
    /* Refresh state information */
    ctl->sret = gst_element_get_state (ctl->pipeline, &ctl->currentState, 
        &ctl->pendingState, GST_CLOCK_TIME_NONE);
 
    /* Show formatted state information */
    /* Current State | State Change Return | */
    fprintf (stdout, "** %-20s : Current State : %-10s | State Change Return : %-10s\n", 
        ctl->description, gst_element_state_get_name (ctl->currentState), 
        gst_element_state_change_return_get_name (ctl->sret));
}

/* Show pipeline runtimes */
void showTimes (gstPipelineControl_t *ctl)
{
    fprintf (stdout, "%s", menuDivider);
    fprintf (stdout, "%s: %s: Time Information\n", prog, ctl->description);
    fprintf (stdout, "%s", menuDivider);

    /* Show current pipeline runtime */
    if (ctl->pipeline != NULL){
        /* Current State | State Change Return | */
        fprintf (stdout, "** %-15s : Base time    : %"GST_TIME_FORMAT "\n", 
            ctl->description,  GST_TIME_ARGS( gst_element_get_base_time (ctl->pipeline) ));
        fprintf (stdout, "** %-15s : Start time   : %"GST_TIME_FORMAT "\n", 
            ctl->description,  GST_TIME_ARGS( gst_element_get_start_time (ctl->pipeline) ));        
        fprintf (stdout, "** %-15s : Running time : %"GST_TIME_FORMAT "\n", 
            ctl->description,  GST_TIME_ARGS( computeRunningTime(ctl)));
    } else {
        fprintf (stdout, "** %-15s: Base time    : N/A\n", ctl->description);
        fprintf (stdout, "** %-15s: Start time   : N/A\n", ctl->description);
        fprintf (stdout, "** %-15s: Running time : N/A\n", ctl->description);
    }
    fprintf (stdout, "%s", menuDivider);
}

/* Show pipeline messages matching the message filter */
void showMessages (GstBus *bus, GstClockTime wait, GstMessageType types, const char *description)
{
    GstMessage *msg = NULL;

    msg = gst_bus_timed_pop_filtered (bus, wait, types);

    if (msg != NULL){
        fprintf (stdout, "%s", menuDivider);
        fprintf (stdout, "%s: %s: Message Log\n", prog, description);
        fprintf (stdout, "%s", menuDivider);       

        while (msg != NULL){

            switch (GST_MESSAGE_TYPE (msg)) {
            case GST_MESSAGE_ERROR:
                {
                    GError *err = NULL;
                    gchar *debug_message = NULL;
                    gst_message_parse_error (msg, &err, &debug_message);
                    fprintf (stderr, "%s: ERROR from element %s: %s\n", 
                        prog, GST_OBJECT_NAME(msg->src), err->message);
                    fprintf (stderr, "%s: - Debug Information  : %s\n", 
                        prog, (debug_message) ? debug_message : "N/A");
                    g_error_free (err);
                    g_free (debug_message);
                }
                break;

            case GST_MESSAGE_EOS:
                fprintf (stdout, "%s: %s: EOS reached\n", 
                    prog, GST_OBJECT_NAME (msg->src));
                break;

            case GST_MESSAGE_STATE_CHANGED:
                {
                    GstState old, new;
                    gst_message_parse_state_changed (msg, &old, &new, NULL);
                    fprintf (stdout, "%s: %s: changed state from %s to %s.\n", 
                        prog, GST_OBJECT_NAME (msg->src), gst_element_state_get_name (old), 
                        gst_element_state_get_name (new));
                }
                break;

            default:
                {
                    fprintf (stdout, "%s: %s: Message (%s) not parsed.\n", 
                        prog, GST_OBJECT_NAME (msg->src), GST_MESSAGE_TYPE_NAME (msg));
                }
                break;
            }
            gst_message_unref (msg);
            //Check for additional messages
            msg = gst_bus_timed_pop_filtered (bus, wait, types);
        } 
        fprintf (stdout, "%s\n", menuDivider);
    }
}
