/* LAB9 Gstreamer Dynamic TPG Example Global Header File */

#ifndef LAB9DYNAMICTPGEXAMPLE_GLOBALS_H_
#define LAB9DYNAMICTPGEXAMPLE_GLOBALS_H_

/* Enable debug print messages in application */
#ifdef DEBUG
    #define debug_print(fmt, ...) fprintf(stderr, "%s:%s:%d: "fmt, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
    #define debug_print(fmt, ...)
#endif

/* Constants for Videotestsrc */
#define LAB9_MIN_TESTPATTERN 0  /* SMPTE 100% colorbars */
#define LAB9_MAX_TESTPATTERN 24 /* Colors */
#define MAX_INPUT_BUFFER 10

/* Type Definitions */
typedef enum {NO_DISPLAY, DISPLAYPORT, HDMI} displaytype_t;
typedef enum {NO_RESOLUTION, RESOLUTION720P, RESOLUTION1080P, RESOLUTION4K} resolutiontype_t;

typedef struct _outputFormat_t {
	int width;
	int height;
	int framerate;
} outputFormat_t;

typedef struct _gstPipelineControl_t {
	GstElement *pipeline;
	GstElement *source;
	GstElement *capsfilter;
	GstCaps *tpg_caps;
	GstElement *queue;
	GstElement *sink;
	GstBus *bus;
	GstMessage *msg;
	GError *err;
	GstClock *clock;
	GstState currentState;
	GstState pendingState;
	GstStateChangeReturn sret;
	const char *description;
} gstPipelineControl_t;

/* Constants */
extern const char *pipelineDescription;
extern const char *drmBusIDHDMI;
extern const char *drmBusIDDP;
extern const char *menuBanner;
extern const char *menuDivider;
extern const char *clearScreen;

/* Global Variables */
extern int display_dp;
extern int display_hdmi;
extern int resolution_720p;
extern int resolution_1080p;
extern int resolution_4k;
extern int menuLevel;
extern const char *prog;

/* Global Function Prototypes */
extern void showMenu (gstPipelineControl_t *pctl, int level);
extern bool processInput (char *input, gstPipelineControl_t *pctl, int *level);
extern void showMessages (GstBus *bus, GstClockTime wait, GstMessageType types, 
	const char *description);
extern void showTimes (gstPipelineControl_t *ctl);
extern void showStateInfo (gstPipelineControl_t *ctl);
extern GstClockTime computeRunningTime (gstPipelineControl_t *ctl);
extern void changePattern (gstPipelineControl_t *ctl, int newpattern);
#endif