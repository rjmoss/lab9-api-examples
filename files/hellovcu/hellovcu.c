/*
* Copyright (C) 2013 - 2016  Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in this
* Software without prior written authorization from Xilinx.
*
*/

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>

/* To turn on debug print messages, uncomment the '#define DEBUG' below this line */
//#define DEBUG

#include "hellovcu.h"

/* Flags set by '--small' and '--large' options */
int messagesize_small = 0;
int messagesize_large = 0;

/* Selected message size */
hellosize_t hellosize = UNDEFINED;

/* Program Name */
const char *prog;

/* Command line long option formatting */
static struct option long_options[] = 
{
	{"size", required_argument, NULL, 's'},
	{"small", no_argument, &messagesize_small, 1},
	{"large", no_argument, &messagesize_large, 1},
	{"help", no_argument, NULL, 'h'},
	{NULL, 0, NULL, 0}
};

int main (int argc, char **argv)
{
	int opt, optcnt = 0;
	char *s_optarg;

    prog = argv[0];

    fprintf (stdout, "%s: Hello VCU Relaunch Session #9\n", prog);

    /* Parse command line options */
    while ((opt = getopt_long(argc, argv, "s:h", long_options, NULL)) != -1) {
        optcnt++;
    	switch (opt) {
    	case 's':
    		s_optarg = optarg;
    		hellosize = getMessageSize (s_optarg);
    		break;
    	
    	case 'h':
    		usage (argv[0]);
    		exit (EXIT_SUCCESS);

    	default:
            if (!checkLongOpts (&opt)) {
    			debug_print ("%s: DEBUG: Unknown option [%c]\n", prog, opt);
    			usage (argv[0]);
                exit (EXIT_FAILURE);
    		}
    		break;
    	}
    }
	
    /* If not options specified, show usage and exit */
    if (optcnt == 0) {
        usage (argv[0]);
        exit (EXIT_SUCCESS);
    }

    if(!showMessage (&hellosize) ){
    	debug_print ("%s: DEBUG: Error - Exiting Program\n", prog);
    	exit (EXIT_FAILURE);
    }

    exit (EXIT_SUCCESS);
}

/* Check long options */
bool checkLongOpts (int *longopt)
{
    bool ret = true;

    debug_print ("%s: DEBUG: Long Option=[%c]\n", prog, *longopt);

    if (messagesize_small){
        debug_print ("%s: DEBUG: Long option --small set\n", prog);
        hellosize = SMALL;
        messagesize_small = 0;
    } else if (messagesize_large){
        debug_print ("%s: DEBUG: Long option --large set\n", prog);
        hellosize = LARGE;
        messagesize_large = 0;
    }
    else {
        debug_print ("%s: DEBUG: Invalid Long option (%c)\n", prog, *longopt);
        ret = false;
    }
    return ret;
}

/* Function to Parse hello message size options */
hellosize_t getMessageSize (char *s_optarg)
{
    hellosize_t hellosize;

    debug_print ("%s: DEBUG: getMessageSize(%s)\n", prog, s_optarg);
    //Process logo display option
    if( strcmp (s_optarg, "small") == 0){
        hellosize = SMALL;
    } else if ( strcmp (s_optarg, "large") == 0) {
        hellosize = LARGE;
    } else {
        hellosize = UNDEFINED;
    }
    return hellosize;
}

/* Function to display message based on size option */
bool showMessage (hellosize_t *size)
{
    const char *hellomessage;
    bool ret = true;

    /* Select hello message size */
    switch (*size) {

        case SMALL:
            debug_print ("%s: DEBUG: Small message\n", prog);
            hellomessage = hellosmall;
            break;

        case LARGE:
            debug_print ("%s: DEBUG: Large message\n", prog);
            hellomessage = hellolarge;
            break;

        case UNDEFINED:
        default:
            debug_print ("%s: DEBUG: Hello message size not specified!\n", prog);
            ret = false;
            break;
    }

    if (ret){
        fprintf (stdout, "%s", hellomessage);
    }

    return ret;
}

/* Command line usage message */
void usage (const char *argv0)
{
    fprintf (stderr, "Usage: %s [options]\n", argv0);
    fprintf (stderr, "Available [options]:\n");
    fprintf (stderr, "\t -s, --size    <size>     Specify the Hello Message Size\n");
    fprintf (stderr, "\t               small\n");
    fprintf (stderr, "\t               large\n");
    fprintf (stderr, "\t --small                 Specify a small Hello Message Size\n");
    fprintf (stderr, "\t --large                 Specify a large Hello Message Size\n");
    fprintf (stderr, "\t -h, --help              Print this help screen and exit\n");
}
