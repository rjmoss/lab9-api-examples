/* Hello VCU Example Global Header File */

#ifndef HELLOVCU_GLOBAL_H_
#define HELLOVCU_GLOBAL_H_

/* Enable debug print messages in application */
#ifdef DEBUG
    #define debug_print(fmt, ...) fprintf (stderr, "%s:%s:%d: "fmt, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
    #define debug_print(fmt, ...)
#endif

/* Type Definitions */
typedef enum {UNDEFINED, SMALL, LARGE} hellosize_t;

/* Constants */
extern const char *hellosmall;
extern const char *hellolarge;

/* Global Variables */
extern int messagesize_small;
extern int messagesize_large;
extern hellosize_t hellosize;
extern const char *prog;

/* Function Prototypes */

#endif